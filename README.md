# README

## API

```
Get metrics for the following levels:
    - /state?id=
    - /district?id=
    - /subdistrict?id=
    - /village?id=
```

## Setup database

1. Pull the latest code and install dependencies `pip install awesome-slugify sqlalchemy psycopg2`
2. Download the latest data from <https://drive.google.com/open?id=0BwaT4clUtYGDUXBrM3AwTmppYTQ> and place it under `data` folder
3. Ensure postgresql works by runnning `psql -U postgres`
4. Run `psql -U postgres -f setup.sql`
5. Run `python database.py`
6. Run the following commands after logging as `map` user with `psql -U map`. Password is `map`.
    ```
    \copy census_states from '/path/to/census_states.csv' delimiter ',' csv header;
    \copy census_districts from '/path/to/census_districts.csv' delimiter ',' csv header;
    \copy census_subdistricts from '/path/to/census_subdistricts.csv' delimiter ',' csv header;
    \copy census_villages from '/path/to/census_villages.csv' delimiter ',' csv header;
    ```
7. Create indices
    ```
    CREATE INDEX census_villages_index ON census_villages (village_code);
    CREATE INDEX census_subdistrict_index ON census_subdistricts (subdistrict_code);
    CREATE INDEX census_district_index ON census_districts (district_code);
    ```

## Convert Shape file to topojson
```
topojson -p DISTRICT,ST_NM -o out.json dist2011_simplified.shp
```
