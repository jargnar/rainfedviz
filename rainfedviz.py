'''
The MIT License (MIT)
Copyright (c) 2016 Suhas SG
'''
import os
import json
import logging
import StringIO
import pandas as pd
from flask import Flask
from slugify import Slugify
from database import connect
from sqlalchemy.sql import select
from flask import request, Response, render_template
from orderedattrdict.yamlutils import AttrDictYAMLLoader
from flask import make_response, jsonify, send_from_directory
from werkzeug.routing import BaseConverter

app = Flask(__name__)


class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


app.url_map.converters['regex'] = RegexConverter
variables = pd.read_csv("data/variables.csv", encoding="utf-8")
filters = variables[['filename', 'varname', 'description']]

logging.basicConfig(
    format='%(asctime)s|%(levelname)s|%(message)s',
    level=logging.INFO
)


con, meta = connect()
slug = Slugify(to_lower=True)
imgs = [
    'https://hd.unsplash.com/photo-1437622368342-7a3d73a34c8f',
    'https://hd.unsplash.com/photo-1435777940218-be0b632d06db',
    'https://hd.unsplash.com/photo-1435777940218-be0b632d06db',
    'https://hd.unsplash.com/photo-1456757014009-0614a080ff7f',
    'https://hd.unsplash.com/photo-1442522772768-9032b6d10e3e',
    'https://hd.unsplash.com/photo-1447826838833-7128dc26d747',
    'https://hd.unsplash.com/photo-1416139129787-60d8314c4708',
    'https://hd.unsplash.com/photo-1427694012323-fb5e8b0c165b',
    'https://hd.unsplash.com/photo-1464454709131-ffd692591ee5'
]
hiers = [
    'filename',
    'subcateg_1',
    'subcateg_2',
    'subcateg_3',
    'subcateg_4',
    'subcateg_5',
    'subcateg_6',
    'subcateg_7'
]

slugvars = variables.fillna('')
for hier in hiers[1:]:
    slugvars[hier] = slugvars[hier].apply(slug)


@app.route('/explore<regex("[1-9a-zA-Z\-_\/]*"):epath>')
def explore(epath):
    '''A really crazy function. This pushes humanity.
    Now on to Mars or something./s'''
    evars = slugvars
    boxes = []
    if not epath:
        for item in list(evars.filename.unique()):
            box = {}
            box['img'] = pd.np.random.choice(imgs)
            box['url'] = '/explore%s/%s' % (epath, item)
            box['name'] = ' '.join(item.split('_'))
            boxes.append(box)
    else:
        parts = epath.split('/')[1:]

        for i, part in enumerate(parts):
            if not part:
                break
            evars = evars[evars[hiers[i]] == part]

        if len(parts) <= 7:
            items = list(evars[hiers[len(parts)]].unique())
            items = items.remove('') if '' in items else items
            if items:
                for item in items:
                    box = {}
                    box['img'] = pd.np.random.choice(imgs)
                    box['url'] = '/explore%s/%s' % (epath, item)
                    box['name'] = ' '.join(item.split('-'))
                    boxes.append(box)

        # also varnames for empty current level evar-hiers
        # ---
        # haha, you didn't understand the above comment did you?
        # hahaha!
        tvars = evars[evars[hiers[len(parts) + 1]] == ''] if len(parts) < 7 else evars        
        for i, row in tvars.iterrows():
            box = {}
            box['img'] = pd.np.random.choice(imgs)
            box['url'] = '/?col=%s&dataset=%s' % (row.varname, row.filename)
            box['name'] = row.description
            boxes.append(box)

    return render_template('explore.html', boxes=boxes)


@app.route('/<dataset>/<level>')
def metrics(dataset, level):
    '''
    Get metrics for the following levels:
        - /state?col=
        - /district?col=
        - /subdistrict?col=
        - /village?col=
    '''
    table, col = '%s_%ss' % (dataset, level), request.args.get('col')
    if not col:
        query = "SELECT column_name FROM information_schema.columns where table_name = '%s'"
        df = pd.read_sql(query % table, con)
        return jsonify(**{
            'help': 'You forgot to pass a ?col= GET parameter to this API. See below.',
            'col': list(df['column_name'])
        })

    hie = {
        'state': 'state', 'district': 'state', 'subdistrict': 'district', 'village': 'subdistrict'}
    par = '%s_code' % hie[level]
    cur = '%s_code' % level
    nam = '%s_name' % level
    val = request.args.get(hie[level])

    # Filter
    try:
        table = meta.tables[table]
    except KeyError:
        return jsonify(**{'error': 'No such table %s exists' % dataset})
    try:
        selec = [table.c[i] for i in [cur, nam, par, col]]
    except KeyError:
        return jsonify(**{'error': 'No such column %s exists in %s' % (col, dataset)})
    claus = select(selec).where(table.c[par] == val) if val else select(selec)

    # Select
    df = pd.read_sql(claus, con)

    if len(df) == 0:
        return jsonify(**{})

    return jsonify(**{'data': df.fillna(0).to_dict(orient='records')})


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(
        os.path.join(app.root_path, 'static'),
        'favicon.ico', mimetype='image/vnd.microsoft.icon'
    )


@app.route('/')
def index():
    dataset = request.args.get('dataset', 'secc')
    defcol = variables[variables.filename == dataset]['varname'].iloc[0]
    return render_template(
        'index.html',
        dataset=dataset,
        selected_col=request.args.get('col', defcol),
        variables=filters
        )


@app.route('/subdistmap')
def getdistmap():
    with open('static/subdist.topo.json') as distmap:
        distmap = json.load(distmap)

    temp = distmap['objects']['subdist_2011_splitcode']['geometries']
    newgeos = []
    for geo in temp:
        t = geo['properties']['dist_code']
        if t == int(request.args.get('code')):
            newgeos.append(geo)

    distmap['objects']['subdist_2011_splitcode']['geometries'] = newgeos
    return jsonify(**distmap)


@app.route('/download/<dataset>/<level>')
def download(dataset, level):
    table, col = '%s_%ss' % (dataset, level), request.args.get('col')

    hie = {
        'state': 'state', 'district': 'state', 'subdistrict': 'district', 'village': 'subdistrict'}
    par = '%s_code' % hie[level]
    cur = '%s_code' % level
    nam = '%s_name' % level
    val = request.args.get(hie[level])

    # Filter
    table = meta.tables[table]
    selec = [table.c[i] for i in [cur, nam, par, col]]
    claus = select(selec).where(table.c[par] == val) if val else select(selec)

    # Select
    _df = pd.read_sql(claus, con)
    si = StringIO.StringIO()
    _df.to_csv(si, index=False)
    output = make_response(si.getvalue())
    output.headers["Content-Disposition"] = "attachment; filename=data.csv"
    output.headers["Content-type"] = "text/csv"
    return output


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True, threaded=True)
