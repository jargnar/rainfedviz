'''
The MIT License (MIT)
Copyright (c) 2016 Suhas SG

postgres=# CREATE DATABASE map;
postgres=# CREATE USER map WITH PASSWORD 'map';
postgres=# GRANT ALL PRIVILEGES ON DATABASE map TO map;
'''
import os
import logging
import sqlalchemy
import pandas as pd
from sqlalchemy import Column, Numeric, Text

logging.basicConfig(
    format='%(asctime)s|%(levelname)s|%(message)s',
    level=logging.INFO
)


def connect():
    '''Returns a connection and a metadata object'''
    url = 'postgresql://map:map@localhost:5432/map'
    con = sqlalchemy.create_engine(url, client_encoding='utf8')
    meta = sqlalchemy.MetaData(bind=con, reflect=True)
    return con, meta


def get_fields_from_csv(f):
    '''Given a csv, converts it into a postgres table'''
    nas = ['N/A', 'NA', 'NULL', 'NaN', 'nan', '"NA"']
    df = pd.read_csv(f, nrows=2, error_bad_lines=False, na_values=nas)

    fields = [
        Column(col, Text)
        if isinstance(df[col].iloc[1], basestring)
        else Column(col, Numeric) for col in df.columns]

    return fields

def get_tablename_from_file(f):
    tblnam = os.path.basename(f).split('.')[0]
    # oh good lord help us for this thing below.
    if tblnam.endswith('dist'):
        tblnam = tblnam + 'rict'
    if not tblnam.endswith('s'):
        tblnam = tblnam + 's'
    return tblnam


def get_level_from_tblnam(tblnam):
    return tblnam.split('_')[-1][:-1]


def setup():
    sqlf = []
    copy_tmpl = '''\copy %s FROM '%s' DELIMITER ',' CSV HEADER NULL AS 'NA';'''
    indx_tmpl = "CREATE INDEX %s_index ON %s (%s_code);"

    con, meta = connect()
    for subdir in list(os.walk('data'))[1:]:
        if os.path.isdir(subdir[0]):
            fs = [os.path.join(subdir[0], f) for f in subdir[2]]
            for f in fs:
                fields, t = get_fields_from_csv(f), get_tablename_from_file(f)
                sqlalchemy.Table(t, meta, *fields, extend_existing=True)
                sqlf.append(copy_tmpl % (t, f))
                sqlf.append(indx_tmpl % (t, t, get_level_from_tblnam(t)))
                logging.info('Created %s' % t)

    meta.create_all(con)

    with open('map.sql', 'w') as outfile:
        outfile.write('\n'.join(sqlf))

    logging.info('Success! Run psql -U map map -f map.sql')

if __name__ == '__main__':
    setup()
